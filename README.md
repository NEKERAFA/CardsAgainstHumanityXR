# Cartas Contra la Humanidad XR

Se ha creado un proyecto en Unity 2022.3.16f1 para implementar la interfaz en realidad aumentada propuesta en la PEC 4 de la asignatura de Diseño y Expereiencia de usuario e interfaces del Máster Universitario en Diseño y Programación de Videojuegos de la UOC. Este proyecto usa el Unity XR Interaction Toolkit para controlar las acciones mediante un periférico VR.

## Índice

- [Como jugar :video_game:](#como-jugar-video_game)
- [Assets de terceros](#assets-de-terceros)

## Como jugar :video_game:

Para empezar una ronda nueva, tenemos que desplazarnos al montón de cartas con preguntas (el montón negro) y hacerle un "grab" a la carta principal [Figura 1](#fig1). Al seleccionarla nos mostrará la carta que hemos cogido con el texto de la pregunta [Figura 2](#fig2). Si la cogemos de nuevo, se lanzará sobre la mesa y pasaremos a nuestro turno.

[<img id="#fig1" src="Extra/figure-1.png" width=640 alt="Figura 1. Inicio de la ronda con las cartas de preguntas."><br>Figura 1. Inicio de la ronda con las cartas de preguntas.](Extra/figure-1.png)

[<img id="#fig2" src="Extra/figure-2.png" width=640 alt="Figura 2. Carta de pregunta de la ronda."><br>Figura 2. Carta de pregunta de la ronda.](Extra/figure-2.png)

En nuestro turno se nos mostrará delate de nosotros la mano con las cartas de respuestas que podemos jugar [Figura 3](#fig3). Del mismo modo que hicimos con la carta de pregunta, seleccionaremos una agarrandola con uno de los controles.

[<img id="#fig3" src="Extra/figure-3.png" width=640 alt="Figura 3. Cartas de la mano del jugador."><br>Figura 3. Cartas de la mano del jugador.](Extra/figure-3.png)

Para terminar turno, robamos una nueva carta de respuestas cogiendola del mazo de preguntas [Figura 4](#fig4). Como con la carta de preguntas, tenemos primero que darle al botón de "Grab" y luego cuando nos salga la carta, agarrarla para incluirla en nuestra mano para finalizar el turno [Figura 5](#fig5).

[<img id="#fig4" src="Extra/figure-4.png" width=640 alt="Figura 4. Mazo de cartas de respuesta"><br>Figura 4. Mazo de cartas de respuesta.](Extra/figure-4.png)

[<img id="#fig5" src="Extra/figure-5.png" width=640 alt="Figura 5. Carta de respuesta robada."><br>Figura 5. Carta de respuesta robada.](Extra/figure-5.png)

Al terminar el turno, juego elige otras 3 cartas simbolizando otros tres jugadores, y al cabo de un rato elige una de las 4 cartas como ganadora, mostrándolas al jugador [Figura 6](#fig6). Una vez terminado, las cartas eliminadas pasan de nuevo al montón de cada mazo y se puede empezar una nueva ronda cogiendo una nueva carta del montón de preguntas.

[<img id="#fig6" src="Extra/figure-6.png" width=640 alt="Figura 6. Fin de la ronda con la carta ganadora."><br>Figura 6. Fin de la ronda con la carta ganadora.](Extra/figure-6.png)

## Assets de terceros

- [Unity XR Interaction Toolkit 2.5.2](https://docs.unity3d.com/Packages/com.unity.xr.interaction.toolkit@2.5/manual/installation.html)
- [Liberation Sans font](https://en.wikipedia.org/wiki/Liberation_fonts)