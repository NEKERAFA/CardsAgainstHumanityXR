using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using static CardController;

public class HandController : MonoBehaviour
{
    public UnityAction<CardController> OnSelectCard;

    [SerializeField]
    List<CardController> cards = new();

    [SerializeField]
    GameManager gameManager;

    int selectedCard;

    Vector3? target = null;
    Vector3 lastPosition;

    void Awake()
    {
        foreach (var card in cards)
        {
            card.SetContent(gameManager.PickContent(CardType.White));
            card.OnSelectCard += SelectCard;
        }
    }

    void Update()
    {
        if (target.HasValue)
        {
            transform.position = MovementHelper.EaseOutQuad(lastPosition, target.Value, transform.position, 0.5f, Time.deltaTime);
        
            if (Vector3.Distance(transform.position, target.Value) < 0.001f)
            {
                transform.position = target.Value;
                target = null;
            }
        }
    }

    public void ShowHand()
    {
        target = Vector3.zero;
        lastPosition = transform.position;
    }

    public void HideHand()
    {
        target = lastPosition;
        lastPosition = transform.position;
    }

    public void PushContent(string content)
    {
        cards[selectedCard].SetContent(content);
    }

    void SelectCard(CardController card)
    {
        selectedCard = cards.FindIndex((obj) => obj.name.CompareTo(card.name) == 0);
        OnSelectCard?.Invoke(card);
    }
}
