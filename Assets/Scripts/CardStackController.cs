using UnityEngine;
using UnityEngine.Events;
using static CardController;

public class CardStackController : MonoBehaviour
{
    public UnityAction<CardType> OnSelectTopCard;
    public UnityAction<CardController> OnConfirmCard;

    [SerializeField]
    CardController selectedCard;

    [SerializeField]
    GameManager gameManager;

    [SerializeField]
    CardType type;

    void Awake()
    {
        selectedCard.OnSelectCard += ConfirmCard;
    }

    public void SelectCard()
    {
        var content = gameManager.PickContent(type);
        selectedCard.SetContent(content);
        var position = selectedCard.transform.position;
        selectedCard.MoveTowards(new Vector3(position.x, 1.15f, position.z));
    }

    public void SelectNewCard()
    {
        OnSelectTopCard?.Invoke(type);
    }

    public void ConfirmCard(CardController card)
    {
        OnConfirmCard?.Invoke(card);
    }
}
