using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class CardController : MonoBehaviour
{
    public enum CardType { Black, White }

    public UnityAction<CardController> OnSelectCard;

    TMP_Text textContent;

    float duration;
    Vector3? target = null;
    Vector3 lastPosition;

    public CardType type;

    void Awake()
    {
        textContent = GetComponentInChildren<TMP_Text>();
    }

    void Update()
    {
        if (target.HasValue)
        {
            transform.position = MovementHelper.EaseOutQuad(lastPosition, target.Value, transform.position, duration, Time.deltaTime);

            if (Vector3.Distance(transform.position, target.Value) < 0.001f)
            {
                transform.position = target.Value;
                target = null;
            }
        }
    }

    public void SetContent(string content)
    {
        textContent.text = content;
    }

    public string GetContent() => textContent.text;

    public void MoveTowards(Vector3 target, float duration = 0.5f)
    {
        this.duration = duration;
        this.target = target;
        lastPosition = transform.position;
    }

    public void ResetLastPosition()
    {
        transform.position = lastPosition;
    }

    public void SelectCard()
    {
        OnSelectCard?.Invoke(this);
    }
}
