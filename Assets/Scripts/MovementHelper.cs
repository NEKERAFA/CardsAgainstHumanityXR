using UnityEngine;

public static class MovementHelper
{
    public static Vector3 EaseOutQuad(Vector3 initialPosition, Vector3 target, Vector3 currentPosition, float duration, float deltaTime)
    {
        var maxDistance = Vector3.Distance(initialPosition, target);
        var currentDistance = Vector3.Distance(currentPosition, target);
        var deltaPosition = (maxDistance - currentDistance) / maxDistance;
        var time = (deltaPosition * duration + deltaTime) / duration;
        return EaseOutQuad(initialPosition, target, time);
    }

    public static Vector3 EaseOutQuad(Vector3 initialPosition, Vector3 target, float deltaTime)
    {
        return Vector3.Lerp(initialPosition, target, EaseOutQuad(deltaTime));
    }

    public static float EaseOutQuad(float step)
    {
        return 1 - (1 - step) * (1 - step);
    }
}
