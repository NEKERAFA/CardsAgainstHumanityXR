using UnityEngine;
using static CardController;

public abstract class GameBehaviour
{
    public abstract void OnStart(GameManager gameManager);
    public abstract void OnUpdate(GameManager gameManager);
    public abstract void OnPickCard(GameManager gameManager, CardType cardStackType);
    public abstract void OnConfirmCard(GameManager gameManager, CardController card);
    public abstract void OnPickHandCard(GameManager gameManager, CardController card);
}

public class OnStartTurnBehaviour : GameBehaviour
{
    bool selectedCard = false;
    CardController blackCard = null;
    float time = 0f;

    public override void OnStart(GameManager gameManager) {}

    public override void OnUpdate(GameManager gameManager)
    {
        if (blackCard != null)
        {
            time += Time.deltaTime;

            if (time > 1f)
            {
                gameManager.ChangeState(new SelectHandCardBehavior());
            }
        }
    }

    public override void OnPickCard(GameManager gameManager, CardType cardStackType)
    {
        if (cardStackType == CardType.Black && !selectedCard)
        {
            gameManager.blackStackController.SelectCard();
            selectedCard = true;
        }
    }

    public override void OnConfirmCard(GameManager gameManager, CardController card)
    {
        if (card.type == CardType.Black && blackCard == null)
        {
            blackCard = gameManager.GetCard(card.transform.position, Quaternion.Euler(90, 0, 0), card.GetContent(), CardType.Black);
            blackCard.MoveTowards(gameManager.blackCardTransform.position, 2f);
            card.ResetLastPosition();
        }
    }

    public override void OnPickHandCard(GameManager gameManager, CardController card) {}
}

public class SelectHandCardBehavior : GameBehaviour
{
    public override void OnStart(GameManager gameManager)
    {
        gameManager.handController.ShowHand();
    }

    public override void OnUpdate(GameManager gameManager) {}

    public override void OnPickCard(GameManager gameManager, CardType cardStackType) {}

    public override void OnConfirmCard(GameManager gameManager, CardController card) {}

    public override void OnPickHandCard(GameManager gameManager, CardController card)
    {
        var cardPosition = gameManager.PickWhiteCardPosition();
        var whiteCard = gameManager.GetCard(card.transform.position, Quaternion.Euler(90, 0, 0), card.GetContent(), CardType.White);
        whiteCard.MoveTowards(cardPosition, 2f);
        gameManager.HideHand();
        gameManager.ChangeState(new SelectNewCardBehaviour());
    }
}

public class SelectNewCardBehaviour : GameBehaviour
{
    bool selectedCard = false;
    bool confirmCard = false;
    float time = 0f;


    public override void OnStart(GameManager gameManager) {}

    public override void OnUpdate(GameManager gameManager)
    {
        if (confirmCard)
        {
            time += Time.deltaTime;

            if (time > 1)
            {
                gameManager.ChangeState(new OnFinishTurnBehaviour());
            }
        }
    }

    public override void OnPickCard(GameManager gameManager, CardType cardStackType)
    {
        if (cardStackType == CardType.White && !selectedCard)
        {
            gameManager.whiteStackController.SelectCard();
            selectedCard = true;
        }
    }

    public override void OnConfirmCard(GameManager gameManager, CardController card)
    {
        if (card.type == CardType.White && !confirmCard)
        {
            gameManager.handController.PushContent(card.GetContent());
            card.ResetLastPosition();

            var topStackPosition = gameManager.whiteStackController.transform.position + new Vector3(0f, 0.08f, 0f);
            for (int i = 0; i < 3; i++)
            {
                var cardPosition = gameManager.PickWhiteCardPosition();
                var whiteCard = gameManager.GetCard(topStackPosition, Quaternion.Euler(90, 0, 0), gameManager.PickContent(CardType.White), CardType.White);
                whiteCard.MoveTowards(cardPosition, 2f);
            }

            confirmCard = true;
        }
    }

    public override void OnPickHandCard(GameManager gameManager, CardController card) {}
}

public class OnFinishTurnBehaviour : GameBehaviour
{
    bool winnerPicked = false;
    float time = 0;

    public override void OnStart(GameManager gameManager) {}

    public override void OnUpdate(GameManager gameManager)
    {
        time += Time.deltaTime;

        if (time > 2 && !winnerPicked)
        {
            time -= 2;

            var card = gameManager.GetSelectedCard(CardType.Black);
            card.transform.rotation = Quaternion.identity;
            var position = card.transform.position + new Vector3(0f, 0.3f, 0f);
            card.MoveTowards(position, 2f);
            card = gameManager.GetSelectedCard(CardType.White);
            card.transform.rotation = Quaternion.identity;
            position = card.transform.position + new Vector3(0f, 0.3f, 0f);
            card.MoveTowards(position, 2f);

            winnerPicked = true;
        }
        else if (time > 5)
        {
            gameManager.ResetCards();
            gameManager.ChangeState(new OnStartTurnBehaviour());
        }
    }

    public override void OnPickCard(GameManager gameManager, CardType cardStackType) {}

    public override void OnConfirmCard(GameManager gameManager, CardController card) {}

    public override void OnPickHandCard(GameManager gameManager, CardController card) {}
}