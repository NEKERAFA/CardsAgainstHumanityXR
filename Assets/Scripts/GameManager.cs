using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static CardController;

public class GameManager : MonoBehaviour
{
    public Transform blackCardTransform;

    [SerializeField]
    List<Transform> whiteCardTransforms = new();
    
    List<Transform> selectedCardTransforms = new();

    public CardStackController blackStackController;

    [SerializeField]
    List<string> blackStack = new();

    public CardStackController whiteStackController;

    [SerializeField]
    List<string> whiteStack = new();

    public HandController handController;

    [SerializeField]
    CardController blackCard;

    [SerializeField]
    List<CardController> whiteCards = new();

    List<CardController> selectedWhiteCards = new();

    GameBehaviour state;

    void Awake()
    {
        whiteStackController.OnSelectTopCard += PickCard;
        whiteStackController.OnConfirmCard += ConfirmCard;
        blackStackController.OnSelectTopCard += PickCard;
        blackStackController.OnConfirmCard += ConfirmCard;
        handController.OnSelectCard += PickHandCard;
        ChangeState(new OnStartTurnBehaviour());
    }

    void Update()
    {
        state?.OnUpdate(this);
    }

    public string PickContent(CardType type)
    {
        if (type == CardType.Black && !blackStack.Any())
        {
            FinishGame();
            return null;
        }

        return PickContent(type == CardType.White ? whiteStack : blackStack);
    }

    string PickContent(List<string> stack)
    {
        var cardPosition = Random.Range(0, stack.Count);
        var content = stack[cardPosition];
        stack.RemoveAt(cardPosition);
        return content;
    }

    public void PushContent(string content, CardType type)
    {
        (type == CardType.White ? whiteStack : blackStack).Add(content);
    }

    public void FinishGame()
    {
        Application.Quit();
        Debug.Log("Exiting application...");
    }

    public void ChangeState(GameBehaviour state)
    {
        this.state = state;
        this.state?.OnStart(this);
    }

    public CardController GetCard(Vector3 position, Quaternion rotation, string content, CardType type)
    {
        var card = type == CardType.Black ? blackCard : PickWhiteCard();
        card.transform.SetPositionAndRotation(position, rotation);
        card.SetContent(content);
        return card;
    }

    CardController PickWhiteCard()
    {
        var card = whiteCards[whiteCards.Count - 1];
        whiteCards.RemoveAt(whiteCards.Count - 1);
        selectedWhiteCards.Add(card);
        return card;
    }

    public Vector3 PickWhiteCardPosition()
    {
        var positionIndex = Random.Range(0, whiteCardTransforms.Count);
        var position = whiteCardTransforms[positionIndex];
        whiteCardTransforms.RemoveAt(positionIndex);
        selectedCardTransforms.Add(position);
        return position.position;
    }

    public void ResetCards()
    {
        blackCard.transform.position = new Vector3(0f, 0f, 1f);
        blackStack.Add(blackCard.GetContent());
        foreach (var whiteCard in selectedWhiteCards)
        {
            whiteCard.transform.position = new Vector3(0f, 0f, 1f);
            whiteStack.Add(whiteCard.GetContent());
        }

        (whiteCardTransforms, selectedCardTransforms) = (selectedCardTransforms, whiteCardTransforms);
        (whiteCards, selectedWhiteCards) = (selectedWhiteCards, whiteCards);
    }

    public void ShowHand()
    {
        handController.ShowHand();
    }

    public void HideHand()
    {
        handController.HideHand();
    }

    public CardController GetSelectedCard(CardType type)
    {
        return type == CardType.White ? GetSelectedWhiteCard() : blackCard;
    }

    CardController GetSelectedWhiteCard() => selectedWhiteCards[Random.Range(0, selectedWhiteCards.Count)];

    void PickCard(CardType cardStackType)
    {
        state?.OnPickCard(this, cardStackType);
    }

    void ConfirmCard(CardController card)
    {
        state?.OnConfirmCard(this, card);
    }

    void PickHandCard(CardController card)
    {
        state?.OnPickHandCard(this, card);
    }
}
